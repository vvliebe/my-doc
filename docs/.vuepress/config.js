module.exports = {
  title: `vvliebe-bookmark`,
  description: '玄雨的书签',
  dest: 'public',
  markdown: {
    toc: { includeLevel: [1, 2, 3] }
  },
  themeConfig: {
    nav: [
      { text: '前端', link: '/FE/' },
      { text: '后端', link: '/backend/' },
    ],
    // sidebar: {
    //   '/FE/': [
    //     ['css/', 'CSS']
    //   ],
    //   '/backend/': [
    //     ['', '首页']
    //   ],
    //   '/home.html': [
    //     ['/FE/', '前端'],
    //     ['/backend/', '后端'],
    //   ]
    // },
    lastUpdated: '上次更新'
  }
}
